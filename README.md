#JSInterface
A simple parser that gives you a REST-like JavaScript interface of a PHP class.

##Create your Controller
Create your Controller by subclassing:

    <?php
    require_once("./jsinterface/interface.php");

    class Foo extends Controller {
      public static function usage() {
        return "Your controller's usage here.";
      }

      public function __construct() {
        // do some initialization
      }

      public function bar() {
        return "return something";
      }

      public function buzz() {
        // or nothing
      }
    }
    ?>

##Usage
Create your own index.php like this:

    <?php
    require_once("./jsinterface/interface.php");
    new JSInterface();
    ?>

or this one if you use session:

    <?php
    session_start();
    require_once("./jsinterface/interface.php");
    new JSInterface();
    ?>

then import the interface js:

    <script type="text/javascript" src="./index.php/<controller_name>/jquery"></script>

As you expect, LongControllerName.longMethod becomes

    ./index.php/long-controller-name/long-method

, case insenstive.

##ToDo
+ list all controller's JavaScript interface
