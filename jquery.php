var <?php echo $agent["name"]?>;

(function($) {
  <?php echo $agent["name"]?> = function() {};

  var _call = function(method, args, done) {
    if($.isFunction(args)) {
      done = args;
      args = {};
    }

    $.ajax({
      url: "<?php echo $agent["path"]?>/<?php echo strtolower($agent["dashed"])?>/" + method,
      type: "POST",
      dataType: "json",
      data: args
    }).done(done);
  };

<?php foreach($agent["methods"] as $method):?>
  <?php echo $agent["name"]?>.prototype.<?php echo $method["name"]?> = function(<?php if(isset($method["params"])) foreach($method["params"] as $param) echo $param.", ";?>done) {
    _call("<?php echo $method["dashed"]?>", {<?php if(isset($method["params"])) foreach($method["params"] as $key => $param) echo ($key !== 0 ? ", " : "")."\"".$param."\": ".$param;?>}, done);
  };

<?php endforeach;?>
})(jQuery);
