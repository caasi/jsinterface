<?php
class NameTools {
  public static function dashedFromCapital($capitalString) {
    preg_match_all("/[A-Z][a-z0-9]*/", $capitalString, $matches);
    return implode("-", array_map("lcfirst", $matches[0]));
  }

  public static function capitalFromDashed($dashedString) {
    return implode("", array_map("ucfirst", array_map("strtolower", explode("-", $dashedString))));
  }
}

class HTTPResponse {
  static function exceptionResponse($statusCode, $message) {
    header("HTTP/1.0 {$statusCode} {$message}");
    echo "{$statusCode} {$message}";
    exit;
  }
}

abstract class Controller {
  private $_agent;

  public static function usage() {
    echo "This is a basic controller.";
  }

  public static function jquery() {
    $script_path = htmlspecialchars($_SERVER["SCRIPT_NAME"]);

    // get_called_class, > 5.3.0 only
    $reflector = new ReflectionClass(get_called_class());

    $agent = array(
      "name" => $reflector->getName(),
      "dashed" => NameTools::dashedFromCapital($reflector->getName()),
      "path" => $script_path,
      "methods" => array()
    );

    foreach($reflector->getMethods(ReflectionMethod::IS_PUBLIC | ReflectionMethod::IS_STATIC) as $method) {
      if(
        $method->name !== "__construct" &&
        $method->name !== "__destruct" &&
        $method->name !== "usage" &&
        $method->name !== "jquery"
      ) {
        $methodReflector = $reflector->getMethod($method->name);

        $params = array();

        foreach($methodReflector->getParameters() as $key => $param) {
          array_push($params, $param->getName());
        }

        array_push(
          $agent["methods"],
          array(
            "name" => $method->name,
            "dashed" => NameTools::dashedFromCapital(ucfirst($method->name)),
            "params" => $params
          )
        );
      }
    }

    header('Content-Type: application/javascript');
    require("jquery.php");
  }
}

class JSInterface extends HTTPResponse {
  private $controller;
  private $segments;

  private function decodeURL() {
    $segments = explode("/", htmlspecialchars($_SERVER["PATH_INFO"]));
    array_shift($segments);
    $segments = array_map(array("NameTools", "capitalFromDashed"), $segments);

    return $segments;
  }

  public function __construct() {
    $this->segments = $this->decodeURL();
    $controllerName = array_shift($this->segments);

    if (!class_exists($controllerName)) {
      $controllerFilePath = $controllerName.".php";
      if (file_exists($controllerFilePath)) {
        require_once($controllerFilePath);
      } else {
        self::exceptionResponse(503, "Service Unavailable.");
      }
    }

    $method = array_shift($this->segments);

    if(!$method || $method === "") {
      $method = "usage";
    }

    $refl = new ReflectionMethod($controllerName, $method);

    if (
      !method_exists($controllerName, $method) ||
      !$refl->isPublic() ||
      $method === "__construct" ||
      $method === "__destruct"
    ) {
      self::exceptionResponse(405, "Method not Allowed.");
    }

    if ($refl->isStatic()) {
      $target = $controllerName;
    } else if ($refl->isPublic()) {
      $target = new $controllerName;
    }
    
    $result = call_user_func_array(array($target, $method), $_POST);

    if (!is_null($result)) {
      echo json_encode($result);
    }
  }

  public function __destruct() {
  }
}
?>
